docker==4.4.4
Flask-BasicAuth==0.2.0
jsonschema==3.2.0
requests==2.28.1
sentry-sdk[flask]==0.20.3

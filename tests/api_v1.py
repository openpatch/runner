import json
import threading
import time
import unittest
from base64 import b64encode

import docker
from instance.config import get_config
from main import app
from openpatch_runner.api.v1.runners.Runner import Runner
from openpatch_runner.api.v1.utils.DockerUtils import DockerUtils
from tests.api_v1_resources import (
    fizzbuzz_mod_src,
    fizzbuzz_mod_test,
    hello_world_src,
    hello_world_test_fail_src,
    hello_world_test_ok_src,
    infloop_src,
)

client = get_config().DOCKER_CLIENT


class TestDockerUtils(unittest.TestCase):
    global client

    def setUp(self):
        self._du = DockerUtils(client, None)

    def tearDown(self):
        pass

    def test_docker_service(self):
        """Tests, whether the docker-service is up and running and whether the passed docker.py-client is valid."""
        self.assertEqual(
            self._du.host_exists(),
            True,
            msg="The docker service could not be reached. Is the docker "
            "service up and running? Is the correct docker.py-client "
            "assigned?",
        )


class TestRunner(unittest.TestCase):
    global client

    def setUp(self):
        self._javarunner = Runner(
            "http://127.0.0.1:" + str(get_config().PORT),
            "registry.gitlab.com/openpatch/runner-java",
            client,
            user="openpatch-runner-api_v1_tests",
        )
        self._javarunner.kill_async(10)
        self._ctnr_id = self._javarunner.get_ctnr_id()

    def tearDown(self):
        try:
            client.api.stop(self._ctnr_id)
        except docker.errors.APIError as e:
            print(e)

    def test_spawn_runner(self):
        """Tests, whether a runner can be spawned on the given docker-host."""
        self.assertEqual(
            client.containers.get(self._ctnr_id).status,
            "running",
            msg="List of running containers does " "not contain runner.",
        )

    def test_kill_runner(self):
        """Tests, whether a java-runner, which executes an infinite-loop, is killed after 10 seconds."""
        data = {"sources": [{"fqcn": "InfLoop", "code": infloop_src}]}
        # Since we created an infinity loop above, the following call will not return.
        # Hence we start it in a thread, so we can simulate this test.
        threading.Thread(target=self._javarunner.run, args=(data,)).start()

        # Wait for 13 seconds. The runner will be terminated after 10.
        time.sleep(13)
        # Make sure, that the runner is not in the list of current running containers anymore.
        # (Should've been killed 3s ago.)
        self.assertNotIn(
            self._ctnr_id,
            client.containers.list(all=True),
            "Container has not been killed as expected."
            "It is imaginable, that the system is under "
            "massive load and the 3-second-difference "
            "to kill a container could not be kept.",
        )

    def test_runner_run_helloworld(self):
        """Tests, whether a java-runner correctly compiles and executes a java-hello-world-program without tests."""
        data = {
            "sources": [
                {
                    "fqcn": "mypkg.HelloWorld",
                    "code": "package mypkg;\n" + hello_world_src,
                }
            ]
        }

        ret = self._javarunner.run(data)
        self.assertNotIn("error", ret, msg="An unexpected error occurred.")
        json_return = ret["payload"]
        self.assertEqual(
            0,
            json_return["src_compile"]["exitcode"],
            msg="Compiler did not exit with return code 0.",
        )
        self.assertEqual(
            "HelloWorld\n",
            json_return["src_run"]["stdout"],
            msg="The HelloWorld-program did not print "
            '"HelloWorld". Have you changed the '
            "source codes? ",
        )

    def test_runner_run_kill_infloop(self):
        """Tests, whether a killed java-runner does return correct exit/return-codes, corresponding to the API."""
        data = {"sources": [{"fqcn": "InfLoop", "code": infloop_src}]}

        ret = self._javarunner.run(data)
        # The runner should have been killed at this point, since run() does not return.
        self.assertIn(
            "error",
            ret,
            msg="An error should have happened but it did not. Was the runner really killed?",
        )
        self.assertNotIn(
            "payload",
            ret,
            msg="There should not be a payload in the return, since the runner does not "
            "produce one (infinite loop), but there is one.",
        )

    def test_runner_run_test_ok(self):
        """Tests, whether a java-runner compiles and runs a sourcecode and a passing junit-test."""
        data = {
            "sources": [
                {
                    "fqcn": "mypkg.HelloWorld",
                    "code": "package mypkg;\n" + hello_world_src,
                }
            ],
            "tests": [
                {
                    "fqcn": "mypkg.HelloWorldTestOk",
                    "code": "package mypkg;\n" + hello_world_test_ok_src,
                }
            ],
        }
        ret = self._javarunner.run(data)
        json_return = ret["payload"]

        self.assertEqual(
            0,
            json_return["test_run"][0]["exitcode"],
            msg="The test compiler has failed " "(non-zero-returncode.)",
        )
        self.assertIn(
            "OK (1 test)",
            json_return["test_run"][0]["stdout"],
            msg="Exactly one java unittest should have "
            "been passed, but something else "
            "happened.",
        )

    def test_runner_run_test_fail(self):
        """Tests, whether a java-runner compiles and runs a sourcecode and a failing junit-test."""
        data = {
            "sources": [
                {
                    "fqcn": "mypkg.HelloWorld",
                    "code": "package mypkg;\n" + hello_world_src,
                }
            ],
            "tests": [
                {
                    "fqcn": "mypkg.HelloWorldTestOk",
                    "code": "package mypkg;\n" + hello_world_test_fail_src,
                }
            ],
        }
        ret = self._javarunner.run(data)
        json_return = ret["payload"]

        self.assertEqual(
            1,
            json_return["test_run"][0]["exitcode"],
            msg="The failed unittest should have produced an "
            "exitcode of '1', but something else happened.",
        )
        self.assertIn(
            "Tests run: 1,  Failures: 1",
            json_return["test_run"][0]["stdout"],
            msg="Exactly one test should "
            "have passed and one "
            "should have failed, but "
            "something else happened",
        )

    def test_runner_run_multi(self):
        """Tests, whether a java-runner runs a complex project with various packages/files and tests."""
        data = {
            "sources": [
                {
                    "fqcn": "myOtherPkg.FizzBuzz",
                    "code": "package myOtherPkg;\nimport mypkg.HelloWorld;\n\n"
                    + fizzbuzz_mod_src,
                },
                {
                    "fqcn": "mypkg.HelloWorld",
                    "code": "package mypkg;\n" + hello_world_src,
                },
            ],
            "tests": [
                {
                    "fqcn": "mytests.HelloWorldTestOk",
                    "code": "package mytests;\nimport mypkg.HelloWorld;"
                    + hello_world_test_ok_src,
                },
                {
                    "fqcn": "mytests.HelloWorldTestFail",
                    "code": "package mytests;\nimport mypkg.HelloWorld;"
                    + hello_world_test_fail_src,
                },
                {
                    "fqcn": "mytests.TestFizzBuzzHW",
                    "code": "package mytests;\nimport myOtherPkg.FizzBuzz;\n"
                    + fizzbuzz_mod_test,
                },
            ],
        }
        ret = self._javarunner.run(data)
        json_return = ret["payload"]

        self.assertEqual(
            0,
            json_return["src_compile"]["exitcode"],
            msg="Source-compiler returned non-zero-exitcode.",
        )
        self.assertEqual(
            0,
            json_return["src_run"]["exitcode"],
            msg="Source-exec returned non-zero-exitcode.",
        )
        self.assertEqual(
            0,
            json_return["test_compile"][0]["exitcode"],
            msg="Test-0-compiler (hello_world_test_ok_src) "
            "returned non-zero-exitcode.",
        )
        self.assertEqual(
            0,
            json_return["test_compile"][1]["exitcode"],
            msg="Test-1-compiler (hello_world_test_fail_"
            "src) returned non-zero-exitcode.",
        )
        self.assertEqual(
            0,
            json_return["test_compile"][2]["exitcode"],
            msg="Test-2-compiler (fizzbuzz_mod_test " "returned non-zero-exitcode.",
        )
        self.assertEqual(
            0,
            json_return["test_run"][0]["exitcode"],
            msg="Test-0, which should have passed, failed.",
        )
        self.assertEqual(
            1,
            json_return["test_run"][1]["exitcode"],
            msg="Test-1, which should have failed, returned " "non-one-exitcode.",
        )
        self.assertEqual(
            1,
            json_return["test_run"][2]["exitcode"],
            msg="Test-2, which should have failed, returned " "non-one-exitcode.",
        )

    def test_runner_run(self):
        """This method tests runner's run()-method with a 100% control flow graph coverage."""
        self._javarunner = None  # We do not need the object from self.setUp()

        data = {
            "sources": [
                {
                    "fqcn": "mypkg.HelloWorld",
                    "code": "package mypkg;\n" + hello_world_src,
                }
            ]
        }

        # 1) Test default timeout from config (no timeout, no kill_async()):
        runner1 = Runner(
            "http://127.0.0.1:" + str(get_config().PORT),
            "registry.gitlab.com/openpatch/runner-java",
            client,
            user="openpatch-runner-api_v1_tests__1",
        )
        ret = runner1.run(data)
        self.assertEqual(
            ret["runner_http_status"], 200, msg="Runner reported non 200 http status."
        )
        payload = ret["payload"]
        self.assertIsInstance(payload, dict, msg="Return is no dict.")
        self.assertEqual(
            payload["src_compile"]["exitcode"], 0, msg="Javac produced an error."
        )
        self.assertEqual(
            payload["src_run"]["exitcode"],
            0,
            msg="Execution of hello world produced an error.",
        )

        # 2) Test timeout as param without async kill (existing timeout, no kill_async()):
        runner2 = Runner(
            "http://127.0.0.1:" + str(get_config().PORT),
            "registry.gitlab.com/openpatch/runner-java",
            client,
            user="openpatch-runner-api_v1_tests__2",
        )
        ret = runner2.run(data, timeout=10)
        self.assertEqual(
            ret["runner_http_status"], 200, msg="Runner reported non 200 http status."
        )
        payload = ret["payload"]
        self.assertIsInstance(payload, dict, msg="Return is no dict.")
        self.assertEqual(
            payload["src_compile"]["exitcode"], 0, msg="Javac produced an error."
        )
        self.assertEqual(
            payload["src_run"]["exitcode"],
            0,
            msg="Execution of hello world produced an error.",
        )

        # 3) Test timeout as param with async kill (existing timeout, kill_async()):
        runner3 = Runner(
            "http://127.0.0.1:" + str(get_config().PORT),
            "registry.gitlab.com/openpatch/runner-java",
            client,
            user="openpatch-runner-api_v1_tests__3",
        )
        runner3.kill_async(10)
        ret = runner3.run(data, timeout=10)
        self.assertEqual(
            ret["runner_http_status"], 200, msg="Runner reported non 200 http status."
        )
        payload = ret["payload"]
        self.assertIsInstance(payload, dict, msg="Return is no dict.")
        self.assertEqual(
            payload["src_compile"]["exitcode"], 0, msg="Javac produced an error."
        )
        self.assertEqual(
            payload["src_run"]["exitcode"],
            0,
            msg="Execution of hello world produced an error.",
        )

        # Destroy all in this test runners:
        runner1.stop()
        runner2.stop()
        runner3.stop()


class TestRoutes(unittest.TestCase):
    def setUp(self):
        self._app = app.test_client()
        self._app.testing = True

    def test_home_status_code(self):
        """Tests, whether Flask returns the correct HTTP-statuscode."""
        # sends HTTP GET request to the application
        # on the specified path
        result = self._app.get("/")

        # assert the status code of the response
        self.assertEqual(
            result.status_code, 200, msg="Flask returned a non-200 http code."
        )

    def test_hmac(self):
        """Tests, whether a HMAC-header is correctly verified by the runner."""
        self.assertIsNot(
            self._app,
            None,
            msg="Initialization of flask test_client() failed for unknown reason.",
        )

        data = {
            "sources": [
                {
                    "fqcn": "mypkg.HelloWorld",
                    "code": "package mypkg;\n" + hello_world_src,
                }
            ]
        }

        # Check lacking HMAC-header:
        req = self._app.post(
            "/api/v1/run",
            data=str(data),
            headers={"info": "This header contains no hmac."},
        )
        self.assertEqual(
            200, req.status_code, msg="Flask returned a non-200 http code."
        )
        self.assertIn(
            "error",
            req.data.decode("utf-8"),
            msg="Flask did not return an error for missing hmac-header.",
        )
        self.assertIn(
            "Request did not contain a HMAC-header.",
            req.data.decode("utf-8"),
            msg="Flask did not return " "an error for missing" "hmac-header.",
        )

        # Check wrong HMAC-header:
        req = self._app.post(
            "/api/v1/run", data=str(data), headers={"HMAC": "~~Some very wrong hash~~"}
        )
        self.assertEqual(200, req.status_code)
        self.assertIn(
            "error",
            req.data.decode("utf-8"),
            msg="Flask did not return an error for wrong hmac-header.",
        )
        self.assertIn(
            "HMAC-header did not match.",
            req.data.decode("utf-8"),
            msg="Flask did not return an error for " "wrong hmac-header.",
        )

        # Sadly, we can not test the correct hmac-headers, since Werkzeug is lacking support to add a header similar to
        # prepared-requests (module: requests). Essentially, we can not calculate the header after preparing a request
        # to Werkzeug and in the corresponding post()-call, since the 'data'-attribute is slightly altering its input.

    def test_diag_java(self):
        """Tests the BasicAuth of the diagnostics page."""
        # More testing is sadly not possible (no fakeable requests)
        # since flask is lacking support for this kind of testing yet.

        self.assertEqual(
            200,
            self._app.get("/").status_code,
            msg="The diagnostics page returned a non 200 http status code.",
        )

        self.assertEqual(
            401,
            self._app.get("/api/v1/diagnostic/java/interface").status_code,
            msg="Is BasicAuth deactivated?",
        )

        # set basicauth to known credentials:
        user = self._app.application.config["BASIC_AUTH_USERNAME"]
        pw = self._app.application.config["BASIC_AUTH_PASSWORD"]
        headers = {
            "Authorization": "Basic "
            + b64encode(bytes(user + ":" + pw, "utf-8")).decode("ascii")
        }

        self.assertEqual(
            200,
            self._app.get(
                "/api/v1/diagnostic/java/interface", headers=headers
            ).status_code,
        )

    def test_schema(self):
        """Tests, whether the json schema can be retrieved."""
        # We only test the status code here, since there are issues with the equalness of the serialized and de-
        # serialized version of the json.
        self.assertEqual(
            200,
            self._app.get("/api/v1/schemas").status_code,
            msg="HTTP-GET to the schema endpoint returned a non 200 http status code.",
        )


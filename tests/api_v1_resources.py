# This file contains various Java-Sourcecodes as python variables for tests.api_v1.py

# Simple Java Hello World
hello_world_src = """
public class HelloWorld {
    private static String msg = "HelloWorld";

    public static void main (String[] args) {
        System.out.println(msg);
    }

    public String getMsg(){
        return msg;
    }
}
"""

# A successful test for Java Hello World
hello_world_test_ok_src = """
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HelloWorldTestOk {
    HelloWorld hw = new HelloWorld();

    @Test
    public void testPrintMessage() {
        assertEquals("HelloWorld", hw.getMsg()); // A successfull test
    }
}
"""

# A failing test for Java Hello World
hello_world_test_fail_src = """
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HelloWorldTestFail {
    HelloWorld hw = new HelloWorld();

    @Test
    public void failTest() {
        assertEquals("HelloFAIL", hw.getMsg()); // A test which will fail
    }
}"""

# A java class with an infinite loop.
infloop_src = """
public class InfLoop {
    public static void main (String[] args) {
        while(true);
    }
}
"""

# The following sourcecode implements the famous FizzBuzz-algorithm.
# It is modified; instead of printing out "FizzBuzz" in certain cases, it will print out "HelloWorld", fetched from
# the HelloWorld-object.
fizzbuzz_mod_src = """
public class FizzBuzz {
    public static void main(String[] args) {
    HelloWorld hw = new HelloWorld();
    
        for (int i = 1; i <= 100; i++) {
            if (i % 15 == 0) {
                System.out.println(hw.getMsg());
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(String.valueOf(i));
            }
        }
    }
    
    public String getStr() {
        HelloWorld hw = new HelloWorld();
        
        String ret = "";
        for (int i = 1; i <= 100; i++) {
            if (i % 15 == 0) {
                ret += hw.getMsg() + '\\n';
            } else if (i % 3 == 0) {
                ret += "Fizz\\n";
            } else if (i % 5 == 0) {
                ret += "Buzz\\n";
            } else {
                ret += String.valueOf(i);
            }
        }
        return ret;
    }
}
"""

# This test succesfully tests, whether the fizzbuzz-class does its job.
fizzbuzz_mod_test = """
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestFizzBuzzHW {
    FizzBuzz fb = new FizzBuzz();

    @Test
    public void testPrintMessage() {
        assertEquals("1\\n2\\nFizz\\n4\\nBuzz\\nFizz\\n7\\n8\\nFizz\\nBuzz\\n11\\nFizz\\n13\\n14\\nHelloWorld\\n" + 
        "16\\n17\\nFizz\\n19\\nBuzz\\nFizz\\n22\\n23\\nFizz\\nBuzz\\n26\\nFizz\\n28\\n29\\nHelloWorld\\n31\\n32\\n" + 
        "Fizz\\n34\\nBuzz\\nFizz\\n37\\n38\\nFizz\\nBuzz\\n41\\nFizz\\n43\\n44\\nHelloWorld\\n46\\n47\\nFizz\\n" + 
        "49\\nBuzz\\nFizz\\n52\\n53\\nFizz\\nBuzz\\n56\\nFizz\\n58\\n59\\nHelloWorld\\n61\\n62\\nFizz\\n64\\n" + 
        "Buzz\\nFizz\\n67\\n68\\nFizz\\nBuzz\\n71\\nFizz\\n73\\n74\\nHelloWorld\\n76\\n77\\nFizz\\n79\\nBuzz\\n" + 
        "Fizz\\n82\\n83\\nFizz\\nBuzz\\n86\\nFizz\\n88\\n89\\nHelloWorld\\n91\\n92\\nFizz\\n94\\nBuzz\\nFizz\\n" + 
        "97\\n98\\nFizz\\nBuzz\\n", fb.getStr());
    }
}
"""

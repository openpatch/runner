# Environment Variables

- OPENPATCH_MODE one of ["development", "production", "testing"]
- OPENPATCH_BASICAUTH_SECRET: Protects the diagnostic interface
- OPENPATCH_HMAC_SECRET: If set the payload send to the runner needs to be hased with this HMAC secret. Defaults to None
- OPENPATCH_MEM_LIMIT: How much memory one concrete runner is allowed to consume. Defaults to 250MB
- OPENPATCH_CPU_PERIOD: Specify the CPU CFS scheduler period, which is used alongside OPENPATCH_CPU_QUOTA. Defaults to 100 micro-seconds. 
- OPENPATCH_CPU_QUOTA: Impose a CPU CFS quota on the container. The number of microseconds per OPENPATCH_CPU_QUOTA that the container is limited to before throttled.
- OPENPATCH_CPU_SHARES: Set this flag to a value greater or less than the default of 1024 to increase or reduce the container’s weight, and give it access to a greater or lesser proportion of the host machine’s CPU cycles.
- OPENPATCH_PROCESSES: How many uswgi process will be spawned. This controll how many concrete runners will run simultaneously. Defaults to 5
- SENTRY_DSN: Used for error tracking

# Testing

```
docker pull registry.gitlab.com/openpatch/runner-java:v1.0.0
docker-compose run runner python3 -m unittest -f -v tests/api_v1.py
```

# Built With

* [Flask](http://flask.pocoo.org/) - Awesome microframework for webdevelopment with Python
* [Docker](http://www.docker.com) - Containerizing applications for security and easy deployment.

# Authors

* **Barkmin, Mike** - *Guidance and conceptualization of the architecture* - https://www.ddi.wiwi.uni-due.de/team/mike-barkmin/
* **Liebers, Jonathan** - *Initial work and realization in WT 2017/18* - https://github.com/jliebers

See https://git.uni-due.de/commoop/commoop-runner for the original work by Jonathan Liebers.

# License

Please see the [LICENSE](LICENSE) file for details.

# FAQs

## Why does the first run take so long?

Do not forget to also install the appropriate images for the concrete runners on the host, so the runner can start them. If they are not present, the runner will pull them, which can take a while.

## Why does the runner sometimes return 500?

It could be possible that the concret runner (e.g. runner-java) needs more than the default 250MB memory. You might need to increase the memory limit for the docker containers, by setting the environmental variable OPENPATCH_MEM_LIMIT.

## How much RAM does the runner consume?

This depends on two factors:

* P: The maximum processes (aka simultaneously connections). This can be set as an env OPENPATCH_PROCESSES.
* M: The memory limit of each container. This can be set as an env OPENPATCH_MEM_LIMIT.
* R: The memory consumtion of the runner.

The upper ceiling (U) of memory consumtion is then calculated as follows:

```math
U = P * M + R
```

See the [wiki](https://gitlab.com/openpatch/runner/-/wikis/Load-Testing) for results of a real load test.

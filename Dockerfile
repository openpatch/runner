FROM registry.gitlab.com/openpatch/flask-microservice-base:v3.7.0

ENV OPENPATCH_ORIGINS=*
ENV OPENPATCH_PROCESSES=20

# Install basic dependencies.
# The installation is intentionally split into two parts, so you can probably reuse the images created by building
# openpatch-runner to save some disk space, since everything except docker is identical.
RUN apt-get update && \
    apt-get -y upgrade

# Install docker. Yes, we install docker in docker. We need to go deeper. (⌐□_□)
RUN apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
RUN curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -
RUN add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
    $(lsb_release -cs) \
    stable"
RUN apt-get update && apt-get -y install docker-ce=17.12.0~ce-0~debian

# All of openpatch-runner will be available under /var/www/.
COPY "." "/var/www/app"
COPY uwsgi.ini /etc/nginx/uwsgi.ini

# Install python dependencies site-wide, since this is an isolated app in a container. We do not make use of a venv.
RUN pip3 install -r /var/www/app/requirements.txt

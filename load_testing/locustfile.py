import json
from locust import HttpUser, between, task


# Simple Java Hello World
hello_world_src = """
public class HelloWorld {
    private static String msg = "HelloWorld";

    public static void main (String[] args) {
        System.out.println(msg);
    }

    public String getMsg(){
        return msg;
    }
}
"""

# A successful test for Java Hello World
hello_world_test_ok_src = """
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HelloWorldTestOk {
    HelloWorld hw = new HelloWorld();

    @Test
    public void testPrintMessage() {
        assertEquals("HelloWorld", hw.getMsg()); // A successfull test
    }
}
"""


class WebsiteRunner(HttpUser):
    wait_time = between(5, 8)

    @task
    def run(self):
        runner_data = {
            "image": "registry.gitlab.com/openpatch/runner-java:v1.0.0",
            "user": "locust",
            "timeout": 600,
            "payload": {
                "sources": [
                    {
                        "fqcn": "mypkg.HelloWorld",
                        "code": "package mypkg;\n" + hello_world_src,
                    }
                ],
                "tests": [
                    {
                        "fqcn": "mypkg.HelloWorldTestOk",
                        "code": "package mypkg;\n" + hello_world_test_ok_src,
                    }
                ],
            },
        }
        response = self.client.post("/api/v1/run", json=runner_data)
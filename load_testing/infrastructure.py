import os
import sys
import time
import subprocess
import pipes
from typing import List
from hcloud import Client
from hcloud.images.domain import Image
from hcloud.servers.client import BoundServer, CreateServerResponse
from hcloud.server_types.client import ServerType


def exists_remote(host, path):
    """Test if a file exists at path on a host accessible with SSH."""
    status = subprocess.call(
        ["ssh", host, "test -f {}".format(pipes.quote(path))],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )
    if status == 0:
        return True
    if status == 1 or status == 255:
        return False


def up(server_type="cx11"):
    client = Client(token=os.getenv("HCLOUD_API_TOKEN"))

    with open("cloud-config.yml", "r") as user_data:
        response: CreateServerResponse = client.servers.create(
            name="load-testing",
            server_type=ServerType(name=server_type),
            image=Image(name="ubuntu-20.04"),
            user_data=user_data.read(),
            ssh_keys=client.ssh_keys.get_all(),
        )
        server: BoundServer = response.server
        return server.public_net.ipv4.ip


def down():
    client = Client(token=os.getenv("HCLOUD_API_TOKEN"))
    server: BoundServer = client.servers.get_by_name("load-testing")
    server.delete()
    print(f"Deleted {server.name}")


if __name__ == "__main__":
    command = sys.argv[1]
    if command == "up":
        try:
            start_time = time.time()
            if len(sys.argv) == 3:
                ip = up(server_type=sys.argv[2])
            else:
                ip = up()

            print(f"Wait for {ip} to become ready")
            while not exists_remote(
                f"root@{ip}", "/var/lib/cloud/instance/boot-finished"
            ):
                print(".", end="", flush=True)
                time.sleep(5)
            print("")
            end_time = time.time()
            print(f"{ip} is up and initialized in {round(end_time - start_time)}s")
        except KeyboardInterrupt:
            print(f"Abort! Start deleting {ip}")
            down()
    elif command == "down":
        down()

import json

import requests
from requests.exceptions import ConnectionError, ReadTimeout

from docker.errors import APIError
from instance.config import get_config
from openpatch_runner.api.v1.utils.DockerUtils import DockerUtils
from openpatch_runner.api.v1.utils.HmacUtils import sign_and_send


class Runner:
    """The Runner-object behaves as a object-oriented-stub to interact with docker.

    Constructing a Runner-object results in a deployed container, which is forcefully killed after a certain timeout.
    This class also acts as a proxy, to interact with the actually deployed container (which is a runner).
    """

    _ctnr_id = None
    _host_url = None
    _du = None
    _user = None
    _logger = None
    _endpoint = None
    _ip = None
    _kill_async_timeout = None
    _mem_limit = None

    def __init__(
        self,
        host_url,
        image,
        client,
        user="unknown",
        logger=None,
        mem_limit=get_config().RUNNER_MEM_LIMIT,
    ):
        """Constructs a runner, thus deploys a container on the docker-host.

        Raises a `RuntimeError`, if said docker-host does not know the `image`.

        :param host_url: String, on which host the Runner is executed.
        :param image: String, which image shall be started on said host.
        :param client: Docker.py-client-object, which is used to deploy the container on said host.
        :param user: String, which user starts this runner (used for logging).
        :param logger: A flask-logger-object. Assign `None` to disable logging (default).
        :param mem_limit: A memory limit as string, which defaults to config.RUNNER_MEM_LIMIT.
        """
        if (
            len(host_url.split(":")) == 3
        ):  # if a host url with two colons is given, i.e. with a port address
            self._host_url = host_url.split(":")[0] + ":" + host_url.split(":")[1]
        else:
            self._host_url = host_url

        self._user = user
        self._du = DockerUtils(client, logger)
        self._mem_limit = mem_limit

        if not self._du.image_exists(image):
            self._du.image_pull(image)

        self._ctnr_id = self._du.start_container(image, self._user)
        self._ip = self._du.get_container_ip(self._ctnr_id)
        self._du.wait_for_runner(self.get_url(), get_config().RUNNER_STARTUP_WAIT)

    def __str__(self):
        """The Runner-object can be cast into a string, to get a short representation of the underlying container-id.

        :return: The first 12 characters of the container id.
        """
        return self._ctnr_id[:12]

    def get_host(self):
        """Returns the hostname of the docker-host.

        :return: The host, on which the container is being executed.
        """
        return self._host_url

    def get_user(self):
        """Returns the username which is associated with this runner.

        :return: Current containers username as string.
        """
        return self._user

    def get_ctnr_id(self):
        """Returns the full container. Cast the runner-object into a string, to get a shorter representation

        :return: The whole id of the underlying container as a string.
        """
        return self._ctnr_id

    def get_url(self):
        """Returns the whole URL of the runner, under which the ReST-Endpoints are registered.

        :return: String to the HTTP-Port of form "http://<hostname>:<portnumber>"
        """
        return "http://" + self._ip

    def get_endpoint(self):
        """Returns the whole URL of the runner's ReST-endpoint.

        It does so, by fetching a json from '/endpoints' on the deployed runner. The json must have an attribute
        "payload", which contains the appropriate endpoint-url, where to send the payload.
        After the first request, the value is cached and reused.

        :return: String to the endpoint of form "http://<hostname>:<portnumber>/api/v1/something"
        """
        if self._endpoint is None:
            req = requests.get(self.get_url() + "/endpoints")
            req_json = json.loads(req.text)
            self._endpoint = req_json["payload"]
        return self.get_url() + self._endpoint

    def run(self, data, timeout=None):
        """Performs an HTTP-POST-Request to the concrete runner, providing it the 'data'-dictionary.

        This operation does not validate the provided data-dictionary. The user of this function must make sure, that
        the dictionary corresponds to the expected format, otherwise the execution will fail.

        :param data: A dictionary, which is issued to the concrete runner ("payload").
                     Please check your manual for the mappings.
        :param timeout: Int, optional. After how many seconds the run-request times out. Defaults to the value set by
        kill_async. If no kill_async_timeout was set, it defaults to instance/config.py-timeout-value.
        :return: A dictionary with the fields 'http_status' and 'content'. Please check your manual for the mappings.
        """
        try:
            req = requests.Request(method="POST", url=self.get_endpoint(), json=data)

            if timeout is None:
                if self._kill_async_timeout is not None:
                    timeout = self._kill_async_timeout
                else:
                    timeout = get_config().RUNNER_DEFAULT_TIMEOUT

            resp = sign_and_send(req, timeout=timeout)
            return {"runner_http_status": resp.status_code, "payload": resp.json()}
        except (ConnectionError, ReadTimeout):
            # A ConnectionError occurs, when the post-request does not finish since the runner was killed
            return {
                "runner_http_status": 500,
                "error": "The runner has been killed (e.g. by timeout).",
            }
        except (RuntimeError, APIError) as e:
            return {"error": str(e)}

    def stop(self):
        """Stops a runner gracefully. This function is non-blocking, i.e. it returns instantly."""
        self._du.stop_container(self._ctnr_id)

    def kill_async(self, timeout):
        """Kills a runner after a certain timeout.

        This function is non-blocking, i.e. it returns instantly.

        This function registers a Thread on the Runner, which will kill it after a certain timeout.
        If the runner stops before reaching that timeout, no exception or notice is thrown (i.e. killed quietly).

        :param timeout: An integer in seconds, after which the runner is killed.
        :return: This function does not return any value.
        """
        self._kill_async_timeout = timeout
        self._du.kill_container(self._ctnr_id, timeout)

import requests

from flask import current_app, jsonify, render_template, request
from flask_basicauth import BasicAuth
from instance.config import get_config
from jsonschema import SchemaError, ValidationError, validate
from openpatch_runner.api.v1 import api
from openpatch_runner.api.v1.runners.Runner import Runner
from openpatch_runner.api.v1.schemas.json_schemas import request_schema
from openpatch_runner.api.v1.utils.HmacUtils import sign_and_send, verify_request

basic_auth = BasicAuth()


@api.route("/run", methods=["POST"])
def run():
    """
    @api {post} /api/v1/run Deploy new runner.
    @apiVersion 1.0.0
    @apiName DeployRunner
    @apiGroup Runner
    @apiHeader {String} HMAC Sha512-hash over the concatenated content with a shared secret. (RFC 2104)
    @apiDescription The main endpoint of OpenPatch-Runner. Each request must provide a JSON, which specifies an
                    'image'-string. This information starts a concrete runner, which lives for 'timeout'-seconds, until
                    it is forcefully terminated. The 'user' is used in the naming of the started concrete runner, which
                    is a docker container. The concrete runner, i.e. the container is forwarded the 'payload'-object,
                    which is processed at the disgrace of the concrete runner.

    @apiPermission api

    @apiParamExample {json} Example Request to OpenPatch-Runner:
            {
                'image': 'commoop-runner-java',
                'user': 'TestUser',
                'timeout': 20,
                'payload':
                    {
                        'sources':
                            [
                                {
                                    'fqcn': 'mypkg.HelloWorld',
                                    'code': 'package mypkg;
                                             public class HelloWorld {
                                                 private static String msg = "HelloWorld";

                                                 public static void main (String[] args) {
                                                     System.out.println(msg);
                                                 }
                                                 public String getMsg(){
                                                     return msg;
                                                 }
                                             }'
                                }
                            ]
                        'test':
                            [
                                {
                                    'fqcn': 'TestHelloWorld',
                                    'code': 'import mypkg.HelloWorld;
                                             import org.junit.Test;
                                             import static org.junit.Assert.assertEquals;

                                             public class HelloWorldTest {
                                                 HelloWorld hw = new HelloWorld();

                                                 [at]Test
                                                 public void testOkay {
                                                     assertEquals("HelloWorld", hw.getMsg());
                                                 }

                                                 [at]Test
                                                 public void failingTestOne() {
                                                     assertEquals("A custom message for the first failing test.",
                                                                  "HelloUniverse", hw.getMsg());
                                                 }

                                                 [at]Test
                                                 public void failingTestTwo() {
                                                     assertEquals("Another custom message for the second failing test.",
                                                                  "HelloCosmos", hw.getMsg());
                                                 }
                                             }'
                                }
                            ]
                    }
            }

    @apiSuccess {json} runnerResult Results, as provided by the deployed concrete runner.
    @apiSuccessExample {json} Example Success-Response for a java-runner:
            {
              "src_compile": {
                "call": "nice -n15 javac -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                "exectime": 2.618844747543335,
                "exitcode": 0,
                "stderr": "",
                "stdout": ""
              },
              "src_run": {
                "call": "nice -n15 java -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                "exectime": 0.23933076858520508,
                "exitcode": 0,
                "stderr": "",
                "stdout": "HelloWorld\n"
              },
              "test_compile": [
                {
                  "call": "nice -n15 javac -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                  "exectime": 2.4677603244781494,
                  "exitcode": 0,
                  "stderr": "",
                  "stdout": ""
                }
              ],
              "test_results": [
                {
                  "is_correct": false,
                  "total_tests_failed": "1",
                  "total_tests_run": "2",
                  "values": [
                    {
                      "actual_value": "Hello[World]",
                      "expected_value": "Hello[FAIL]",
                      "message": "",
                      "number": "1"
                    }
                  ]
                }
              ],
              "test_run": [
                {
                  "call": "nice -n15 java -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                  "exectime": 0.7232999801635742,
                  "exitcode": 1,
                  "stderr": "",
                  "stdout": "JUnit version 4.12\n.E.\n [...] \nTests run: 2,  Failures: 1\n\n"
                }
              ]
            }
    """

    def handle_keyerror(keyerror: KeyError):  # lambda for error handling
        return jsonify(
            {
                "error": str(keyerror),
                "type": "KeyError",
                "errormsg": "You might be missing a variable in your JSON (see 'error').",
            }
        )

    # Verify HMAC:
    try:
        verify_request(request)
    except RuntimeError as e:
        return jsonify({"error": str(e)})

    post = request.get_json()

    # Validate Schema:
    try:
        validate(post, request_schema)
    except (ValidationError, SchemaError) as ve:
        get_config().LOGGER.debug(
            "An exception occurred during schema validation!\n" + str(ve)
        )
        return jsonify({"error": str(ve)})

    # Deploy runner:
    try:
        runner = Runner(
            request.url_root,
            post["image"],
            get_config().DOCKER_CLIENT,
            logger=get_config().LOGGER,
            user=post["user"],
        )
    except KeyError as ke:
        return handle_keyerror(ke)

    try:
        runner.kill_async(post["timeout"])
    except KeyError as ke:
        runner.kill_async(0)
        return handle_keyerror(ke)

    if get_config().LOGGER is not None:
        get_config().LOGGER.debug(
            "Started container commoop-runner-java"
            + " with id "
            + str(runner)
            + " for "
            + runner.get_user()
            + "\n"
            + "Deployed runner can be reached at: "
            + runner.get_url()
        )
    # Get return of Runner
    try:
        result = runner.run(post["payload"], post["timeout"])
    except KeyError as ke:
        return handle_keyerror(ke)

    # Stop runner gracefully:
    runner.stop()

    if get_config().LOGGER is not None:
        get_config().LOGGER.debug(
            f"Stopping container {post['image']} with id " + str(runner)
        )
    return jsonify(result)


@api.route("/schemas")
def get_schemas():
    """
    @api {get} /schemas Get JSON-schema.
    @apiVersion 1.0.0
    @apiName GetSchema
    @apiGroup Runner
    @apiDescription Returns the request_schema of comoop-runner.

    @apiPermission api

    @apiSuccess {json} request_schema JSON-Schema, against which all requests to the Runner are validated.
    :return: request_schema as dict
    """
    return jsonify({"request_schema": request_schema})


@api.route("/diagnostic/java/interface", methods=["GET", "POST"])
@basic_auth.required
def diagnostic_interface():
    """
    @api {post} /diagnostic/java/interface Use diagnostic frontend for debugging.
    @apiVersion 1.0.0
    @apiName DiagnosticInterface
    @apiGroup Runner
    @apiDescription This endpoint can be used only with BasicAuth, which is easier to set up than HMAC.
                    It emulates /run. The fields differ to little extend from /run. Please see the documentation,
                    especially appendix A.5 for more information, how to use cURL for example. This endpoint is easier
                    for debugging, because the constant recomputation of a HMAC-header can become quite exhaustive.
    @apiHeader {String} Authorization BasicAuth (RFC 7617). User and password is set in configuration.

    @apiParamExample {json} Example Request to OpenPatch-Runner:
            {
                'seconds': '20',
                'payload':
                    {
                        'sources':
                            [
                                {
                                    'fqcn': 'mypkg.HelloWorld',
                                    'code': 'package mypkg;
                                             public class HelloWorld {
                                                 private static String msg = "HelloWorld";

                                                 public static void main (String[] args) {
                                                     System.out.println(msg);
                                                 }
                                                 public String getMsg(){
                                                     return msg;
                                                 }
                                             }'
                                }
                            ]
                        'test':
                            [
                                {
                                    'fqcn': 'TestHelloWorld',
                                    'code': 'import mypkg.HelloWorld;
                                             import org.junit.Test;
                                             import static org.junit.Assert.assertEquals;

                                             public class HelloWorldTest {
                                                 HelloWorld hw = new HelloWorld();

                                                 [at]Test
                                                 public void testOkay {
                                                     assertEquals("HelloWorld", hw.getMsg());
                                                 }

                                                 [at]Test
                                                 public void failingTestOne() {
                                                     assertEquals("A custom message for the first failing test.",
                                                                  "HelloUniverse", hw.getMsg());
                                                 }

                                                 [at]Test
                                                 public void failingTestTwo() {
                                                     assertEquals("Another custom message for the second failing test.",
                                                                  "HelloCosmos", hw.getMsg());
                                                 }
                                             }'
                                }
                            ]
                    }
            }

    @apiSuccess {json} runnerResult Results, as provided by the deployed concrete runner.
    @apiSuccess {json} runnerResult['payload'] Payload
    @apiSuccessExample {json} Example Success-Response for a java-runner:
            {
                'http_status': 200,
                'payload': {
                  "src_compile": {
                    "call": "nice -n15 javac -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                    "exectime": 2.618844747543335,
                    "exitcode": 0,
                    "stderr": "",
                    "stdout": ""
                  },
                  "src_run": {
                    "call": "nice -n15 java -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                    "exectime": 0.23933076858520508,
                    "exitcode": 0,
                    "stderr": "",
                    "stdout": "HelloWorld\n"
                  },
                  "test_compile": [
                    {
                      "call": "nice -n15 javac -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                      "exectime": 2.4677603244781494,
                      "exitcode": 0,
                      "stderr": "",
                      "stdout": ""
                    }
                  ],
                  "test_results": [
                    {
                      "is_correct": false,
                      "total_tests_failed": "1",
                      "total_tests_run": "2",
                      "values": [
                        {
                          "actual_value": "Hello[World]",
                          "expected_value": "Hello[FAIL]",
                          "message": "",
                          "number": "1"
                        }
                      ]
                    }
                  ],
                  "test_run": [
                    {
                      "call": "nice -n15 java -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                      "exectime": 0.7232999801635742,
                      "exitcode": 1,
                      "stderr": "",
                      "stdout": "JUnit version 4.12\n.E.\n [...] \nTests run: 2,  Failures: 1\n\n"
                    }
                  ]
                }
            }

    @apiPermission api
    @apiPermission admin
    """
    if request.method == "GET":
        # Serve static page
        return render_template("diagnostic/java/interface.html")
    if request.method == "POST":
        # We received the POST-XHR and create a response.
        # We use this pattern (i.e. passing variables via post) so we do not have to send the hmac-secret to the client.

        # if app is run in debug mode (w/o nginx) send to url with custom port (in host_url)
        if current_app.debug:
            url = request.host_url
        else:  # else send them from within the container to localhost
            url = "http://localhost/"

        # Prepare request
        prep_req = requests.Request(
            "POST",
            url + "api/v1/run",
            json={
                "image": "registry.gitlab.com/openpatch/runner-java",
                "user": "diagnostic-interface",
                "payload": request.get_json(),
                "timeout": int(request.get_json()["seconds"]),
            },
        )

        # Return content of response
        try:
            response = sign_and_send(
                prep_req, timeout=int(request.get_json()["seconds"])
            )
        except requests.exceptions.ReadTimeout:
            # A ConnectionError occurs, when the post-request does not finish since the runner was killed
            return jsonify(
                {"runner_http_status": 500, "error": "The runner has been killed."}
            )
        return response.content

import os
from flask import Blueprint
from flask_cors import CORS

# Define Blueprint
api = Blueprint("api", __name__, template_folder="templates", static_folder="static")

CORS(api, origins=[os.getenv("OPENPATCH_ORIGINS", "*")])

# Import all components
# These import statements MUST be placed below the blueprint-definition
# (Do not move the following line.)
from . import routes

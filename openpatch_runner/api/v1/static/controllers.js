function escapeHTML(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }

function loadPreset() {
    var e = document.getElementById('preset');
    var preset = e.options[e.selectedIndex].value;

    if (preset === 'helloworld') {
        var source1 = `package mypkg;

public class HelloWorld {
    private static String msg = "HelloWorld";

    public static void main (String[] args) {
        System.out.println(msg);
    }

    public String getMsg(){
        return msg;
    }
}
        `;

        var test1source=`import mypkg.HelloWorld;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HelloWorldTest {
    HelloWorld hw = new HelloWorld();

    @Test
    public void testPrintMessage() {
        assertEquals("HelloWorld", hw.getMsg());
    }

    @Test
    public void failTest() {
        assertEquals("HelloFAIL", hw.getMsg());
    }
}`;

        var test1name = 'HelloWorldTest';

        var source1name = 'mypkg.HelloWorld';
    } else if (preset === 'fizzbuzz') {
        var source1 = `public class FizzBuzz {
        public static void main(String[] args) {
            for (int i = 1; i <= 100; i++) {
                if (i % 15 == 0) {
                    System.out.println("FizzBuzz");
                } else if (i % 3 == 0) {
                    System.out.println("Fizz");
                } else if (i % 5 == 0) {
                    System.out.println("Buzz");
                } else {
                    System.out.println(String.valueOf(i));
                }
            }
        }
    }`
        var source1name = 'FizzBuzz';
        var test1name = '';
        var test1source = '';
    } else if (preset ==='infloop') {
        var source1 = `public class InfLoop {
        public static void main(String[] args) {
            while(true); // infloop
        }
    }`;
        var source1name = 'InfLoop';
        var test1name = '';
        var test1source = '';
    }

    document.getElementById('sourcefqcn1').value = source1name;
    document.getElementById('sourcecode1').value = source1;
    document.getElementById('testfqcn1').value = test1name;
    document.getElementById('testcode1').value = test1source;
}


function sendJson() {
    var sources = [];
    var tests = [];

    // fetch all data from forms
    for (var i = 0; i < document.getElementById("sources").children.length; i++){
        sources.push(
            {
                "fqcn": document.getElementById("sources").children[i].children.namedItem("sourcefqcn" + (i+1)).value,
                "code": document.getElementById("sources").children[i].children.namedItem("sourcecode" + (i+1)).value
            }
        );
    }
    for (var i = 0; i < document.getElementById("tests").children.length; i++){
        if (document.getElementById("tests").children[i].children.namedItem("testfqcn" + (i+1).value) !== '') {
            tests.push(
                {
                    "fqcn": document.getElementById("tests").children[i].children.namedItem("testfqcn" + (i+1)).value,
                    "code": document.getElementById("tests").children[i].children.namedItem("testcode" + (i+1)).value
                }
            );
        }
    }

    // compile payload
    var payload = {
        "seconds": document.getElementById("sec").value,
        "sources": sources,
        "tests": tests,
    };

    var oReq = new XMLHttpRequest();
    oReq.open("POST", "", true);
    oReq.setRequestHeader("Content-type", "application/json");

    oReq.onload = function() {
        //if (this.readyState === 4 && this.status === 200) {
            var resp = JSON.parse(this.responseText);

            if (resp['error']) {
                http_status.innerHTML = '(code ' + resp["runner_http_status"] + ')';
                content.innerHTML = 'ERROR: ' + resp['error'];
            } else {
                http_status.innerHTML = '(code ' + resp["runner_http_status"] + ')';
                content.innerHTML = escapeHTML(resp['payload']);
                content.innerHTML = JSON.stringify(JSON.parse(content.innerHTML), null, 2);
            }
        //}
    };

    oReq.send(JSON.stringify(payload));
    // end of xhr

    var content = document.getElementById('content');
    var http_status = document.getElementById('http_status');
    http_status.innerHTML = '';
    content.innerHTML = 'Request sent to commoop-runner-java via commoop-runner. Waiting for response.';
}

function cloneFormfield(parentTag){
    if (parentTag == "sources"){
        var prefix = "source";
        var title = "Class";
    } else if (parentTag === "tests"){
        var prefix = "test";
        var title = "Test";
    } else {
        console.log('JS-ERROR: parentTag must be "sources" or "tests"!');
    }

    var itm = document.getElementById(parentTag).getElementsByTagName('p')[0];
    var cln = itm.cloneNode(true);

    var newIndex = itm.parentElement.childElementCount + 1;
    cln.children.namedItem("title").innerHTML = title + ' ' + newIndex;
    cln.children.namedItem(prefix + "fqcn").id = prefix + 'fqcn' + newIndex;
    cln.children.namedItem(prefix + "code").id = prefix + 'code' + newIndex;

    document.getElementById(parentTag).appendChild(cln);
}

function removeFormfield(parentTag){
    var itm = document.getElementById(parentTag);

    if (itm.children.length <= 1){
        return;
    } else {
        itm.removeChild(itm.lastChild);
    }
}

function addSource() {
    cloneFormfield("sources");
}

function remSource() {
    removeFormfield("sources");
}

function addTest() {
    cloneFormfield("tests");
}

function remTest() {
    removeFormfield("tests");
}
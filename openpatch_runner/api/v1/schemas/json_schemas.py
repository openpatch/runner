# This file contains two schemas, against which all requests and responses are validated.

request_schema = {
    "$schema": "http://json-schema.org/draft-04/schema",
    "type": "object",
    "properties": {
        "image": {"type": "string"},
        "payload": {"type": "object"},
        "user": {"type": "string"},
        "timeout": {"type": "integer"},
    },
    "required": ["image", "payload", "user", "timeout"],
}

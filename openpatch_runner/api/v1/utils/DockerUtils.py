import threading
import time
from datetime import datetime

import requests
from requests.exceptions import ConnectionError, HTTPError

from docker.errors import APIError
from instance.config import get_config


class DockerUtils:
    """DockerUtils encapsulates various functions of the high- and low-level-api of docker.py

    It is mainly used for convenience and to provide functions to the Runner-class.
    """

    def __init__(self, client, logger):
        """Constructs a Docker-Utils-object, which encapsulates the high- and low-level-api of docker.py

        The __init__-function checks, whether the client is usable. If the host does not exist (i.e. it can not be
        pinged), a RuntimeError will be raised.

        :param client: Expects a docker.py-client-object which is used to execute all requests against docker-daemon.
        :param logger: Expects a (Flask.logger)-object, which is used for all logging. Assign 'None' to disable logging.
        """
        self._client = client
        self._logger = logger
        self._user = None
        self._name = None

        if client is None or not self.host_exists():
            raise RuntimeError(
                "DockerUtils could not be initialized. Please check your assigned docker client "
                "and whether the docker-service is up and running."
            )

    def start_container(self, image, user, mem_limit=get_config().RUNNER_MEM_LIMIT):
        """Starts a container in detached-mode, which will be removed after termination.

        A maximum of 100m is assigned per default for memory. Also, a random port-mapping to 80/tcp is done by the host.
        To get the randomly assigned port, use "get_container_runner_port()".

        :param image: String of the base-image, which is launched.
        :param user: String, which user has started the container. Will be appended to container's name.
        :param mem_limit: String, how many RAM is assigned to the runner. Defaults to "100m".
        :return: String of the container's id.
        :param mem_limit: A memory limit as string, which defaults to config.RUNNER_MEM_LIMIT.
        """
        self._user = user
        self._name = (
            self._user + "_" + datetime.utcnow().strftime("%Y-%m-%d_%H-%M-%S.%f")[:-3]
        )

        # Start container
        ctnr_id = self._client.containers.run(
            image=image,
            ports={"80/tcp": None},
            detach=True,
            auto_remove=True,
            mem_limit=mem_limit,
            name=self._name,
            environment={"OPENPATCH_HMAC_SECRET": get_config().OPENPATCH_HMAC_SECRET},
            cpu_period=get_config().RUNNER_CPU_PERIOD,
            cpu_quota=get_config().RUNNER_CPU_QUOTA,
            cpu_shares=get_config().RUNNER_CPU_SHARES,
        ).id

        if self._logger is not None:
            self._logger.debug(
                "Starting container with id "
                + ctnr_id
                + " for user "
                + self._user
                + "."
            )

        # Connect container to network:
        if get_config().RUNNER_NETWORK_NAME is not None:
            self.connect_container(ctnr_id, get_config().RUNNER_NETWORK_NAME)
        elif self._logger is not None:
            self._logger.warn(
                "No network provided in instance/config.py. Spawned container can not be connected "
                "to any network!!"
            )
        return ctnr_id

    def stop_container(self, ctnr_id):
        """Gracefully stops a container.

        This function is non-blocking; it starts a thread to take care of the container and returns immediately.

        :param ctnr_id: String, which container to stop (container-id).
        :return: Returns immediately without any value.
        """
        if self._logger is not None:
            self._logger.debug(
                "Stopping container with id " + ctnr_id[:12] + " of user " + self._user
            )
        threading.Thread(target=self._stop_container, args=(ctnr_id,)).start()

    def kill_container(self, ctnr_id, timeout):
        """Kills a container after a certain timeout..

        This function is non-blocking; it starts a thread to take care of the container and returns immediately.

        :param ctnr_id: String, which container to kill (container-id).
        :param timeout: Integer, after how many seconds the container is forcefully killed.
        :return: Returns immediately without any value.
        """
        threading.Thread(target=self._kill_container, args=(ctnr_id, timeout)).start()

    def connect_container(self, ctnr_id, network_name, aliases=[]):
        """Connects a container do a docker network.

        :param ctnr_id: String, which container shall be connected.
        :param network_name: String, which network the container shall be connected to.
        :param aliases: List, with docker network aliases. Defaults to empty list.
        """
        network = self._client.networks.list(names=[network_name])
        if len(network) == 0 and self._logger is not None:
            self._logger.warn(
                "No docker-networks found. Please check your entry to instance/config.py. It must "
                "match exactly one (1) network."
            )
        elif len(network) > 1 and self._logger is not None:
            self._logger.warn(
                "Multiple docker-networks found. Your entry to instance/config.py must match exactly "
                "exactly one (1) network. CONNECTING to first result, id="
                + network[0].id
            )
        network[0].connect(ctnr_id, aliases=aliases)

    def disconnect_container(self, ctnr_id, network_name):
        """Disconnects a container from a docker network.

        :param ctnr_id: String, which container shall be disconnected.
        :param network_name: String, which network the container shall be disconnected from.
        """
        network = self._client.networks.list(names=[network_name])
        if len(network) == 0 and self._logger is not None:
            self._logger.warn(
                "No docker-networks found. Please check your entry to instance/config.py. It must "
                "match exactly one (1) network."
            )
        elif len(network) > 1 and self._logger is not None:
            self._logger.warn(
                "Multiple docker-networks found. Your entry to instance/config.py must match exactly "
                "exactly one (1) network. DISCONNECT from first result, id="
                + network[0].id
            )
        network[0].disconnect(ctnr_id)

    def _stop_container(self, ctnr_id):
        """Helper-function intended as thread-target to stop the container."""
        try:
            self.disconnect_container(ctnr_id, get_config().RUNNER_NETWORK_NAME)
            self._client.api.stop(ctnr_id, timeout=0)
        except APIError as e:
            if self._logger is not None:
                self._logger.warn(str(e))

    def _kill_container(self, ctnr_id, timeout):
        """Helper-function intended as thread-target to kill the container."""
        try:
            time.sleep(timeout)
            self.disconnect_container(ctnr_id, get_config().RUNNER_NETWORK_NAME)
            self._client.api.kill(ctnr_id)
            if self._logger is not None:
                self._logger.warn(
                    "KILLED container "
                    + ctnr_id
                    + " of user "
                    + self._user
                    + " after timeout of "
                    + str(timeout)
                    + " sec."
                )
        except (ConnectionError, HTTPError, APIError):
            # We drop the occurring Docker-HTTP-500-Error.
            # The container has been gracefully stopped by now.
            pass

    def get_container_ip(self, ctnr_id):
        """Gets the IP of the container in the docker-network, which is specified in instance/config.py.

        :param ctnr_id: String, the container-id.
        :return: String, which ip-address has been assigned.
        """
        network_name = get_config().RUNNER_NETWORK_NAME
        return self._client.api.inspect_container(ctnr_id)["NetworkSettings"][
            "Networks"
        ][network_name]["IPAddress"]

    def wait_for_runner(self, url, initial_wait=10):
        """Waits for a runner-image to start fully up and be ready to serve requests.

        This function blocks, until the runner is fully up and ready. Raises a RuntimeError, if the container is not
        reachable after `intial_wait`-seconds.

        :param url: String, which url to poll.
        :param initial_wait: int, how many seconds this function should block at most (defaults to 10).
        :return: Blocks until the container is ready to serve requests. Returns without any value.
        """
        # Wait until docker container commoop-runner-java is ready to serve requests
        for i in range(0, initial_wait):
            try:
                if requests.head(url).status_code == 200:
                    return
            except requests.ConnectionError:
                # We cure the ConnectionError connection by waiting for container to start up
                time.sleep(1)
            if i >= initial_wait:
                raise RuntimeError(
                    "Could not reach spawned runner at url "
                    + url
                    + " after "
                    + self._timeout
                    + " retries."
                )

    def image_exists(self, imagetag):
        """ Checks whether an image exists on given host."""
        lst = [str(x["RepoTags"]) for x in self._client.api.images()]

        for l in lst:
            if imagetag in l:
                return True
        return False

    def image_pull(self, imagetag):
        self._client.api.pull(imagetag)

    def host_exists(self):
        try:
            return self._client.ping()
        except ConnectionError as e:
            if self._logger is not None:
                self._logger.warn(
                    "Could not reach container. Is the docker service running and reachable?"
                )
            print("Fatal error: " + str(e))
            return False

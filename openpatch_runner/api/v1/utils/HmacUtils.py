import hashlib
import hmac

import requests

from instance.config import get_config


def sign_request(request: requests.Request) -> requests.PreparedRequest:
    """Signs a requests.request-object by adding a HMAC-header, which is created over the `.data`-attribute.

    The `.data`-attribute shall be encoded with `utf-8`, i.e. it must not contain binary data, otherwise it can not
    be signed. After the 'HMAC'-header has been added, you can use the following pattern, to send the request:
    `response = requests.Session().send(signed_prep_req)`.

    Alternatively, you can use `HmacUtils.sign_and_send()`.

    Attention: If no OPENPATCH_HMAC_SECRET-environment variable is set, a log entry is created and no header is added.

    :param request: An unprepared `requests.request` object.
    :return: A prepared `requests.request` object, which has a HMAC-header.
    """
    prep_req = request.prepare()

    if get_config().OPENPATCH_HMAC_SECRET is None:
        if get_config().LOGGER is not None:
            get_config().LOGGER.warning(
                "OPENPATCH_HMAC_SECRET environment variable is not set.\n"
                "HMAC is disabled.\n"
                "Your API-endpoint is open to all requests."
            )
        return prep_req
    else:
        prep_req.headers["HMAC"] = hmac.new(
            bytes(get_config().OPENPATCH_HMAC_SECRET, "utf-8"),
            bytes(str(prep_req.body), "utf-8"),
            hashlib.sha512,
        ).hexdigest()
        return prep_req


def verify_request(request: requests.Request) -> None:
    """Verifies a given request-object due to the HMAC-principle.

    This function verifies a request-object by checking `request.headers` for a `HMAC`-string. The header shall contain
    a SHA512-hash (hashed with a secret shared key). This hash then is compared with `request.data`-attribute.

    In case the hashes match, this function does nothing. Otherwise a `RuntimeError` is raised, if the hash is wrong
    or the header is absent.

    The `request.data`-attribute shall be encoded with `utf-8`, i.e. it must not contain binary data.

    Attention: If no OPENPATCH_HMAC_SECRET-environment variable is set, the HMAC-Header will not be validated.

    :param request: A request-object, which must contain a `.header` list and `.data` bytes-attribute.
    :raises RuntimeError: Either no HMAC-header was sent or the hashes did not match.
    """
    if get_config().OPENPATCH_HMAC_SECRET is None:
        if get_config().LOGGER is not None:
            get_config().LOGGER.warning(
                "OPENPATCH_HMAC_SECRET environment variable is not set.\n"
                "HMAC is disabled.\n"
                "Your API-endpoint is open to all requests."
            )
        return

    digest_maker = hmac.new(
        bytes(get_config().OPENPATCH_HMAC_SECRET, "utf-8"),
        bytes(str(request.data), "utf-8"),
        hashlib.sha512,
    )
    if "HMAC" not in request.headers:
        raise RuntimeError("Request did not contain a HMAC-header.")
    elif not hmac.compare_digest(request.headers["HMAC"], digest_maker.hexdigest()):
        raise RuntimeError("HMAC-header did not match.")


def sign_and_send(
    request: requests.Request, timeout=get_config().RUNNER_DEFAULT_TIMEOUT
) -> requests.Response:
    """Signs and sends a prepared requests.request-object.

    This is a wrapper to sign_request() in combination with opening a session and sending the message in one line.

    :param request: A  request, which will be signed and sent.
    :param timeout: (Optional) (Int) The timeout, after which a sent request timeouts. Defaults to 20.
    :return (requests.Response) Response
    :raises requests.exceptions.ReadTimeout: when a runner is killed.
    """

    return requests.Session().send(sign_request(request), timeout=timeout)

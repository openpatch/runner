from flask import Flask, jsonify
from flask_cors import cross_origin

# local import
from instance.config import get_config

# routes
from .api.v1 import api as api_v1


def create_app():
    """
    Creates a flask-app and registers the corresponding blueprint.
    :return: the app, with the all routes from the blueprint registered.
    """
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(get_config())

    # register api endpoints
    app.register_blueprint(api_v1, url_prefix="/api/v1")

    @app.route("/healthcheck", methods=["GET"])
    @cross_origin()
    def healthcheck():
        return jsonify({"msg": "ok"}), 200

    return app

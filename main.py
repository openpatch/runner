import os
from flask import render_template
from instance.config import get_config
from openpatch_runner import create_app
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

app = create_app()
app.config.from_object(get_config())
get_config().LOGGER = app.app_context().app.logger


sentry_dsn = os.getenv("SENTRY_DSN", None)
environment = os.getenv("OPENPATCH_MODE", "development")

if sentry_dsn:
    sentry_sdk.init(
        dsn=sentry_dsn, integrations=[FlaskIntegration()], environment=environment
    )


@app.route("/")
def index():
    """Generates the landing page (index)."""
    pages = []
    for rule in app.url_map.iter_rules():
        if "static" in rule.rule:
            continue
        pages.append([rule.rule, "[" + ", ".join(sorted(list(rule.methods))) + "]"])

    pages.sort()
    pages.insert(0, ["<url>", "<http-methods>"])

    # make a nice table of all flask rules by generating spaces:
    spacing = 4
    col = []
    col_len = []
    for k in range(0, len(pages[0])):
        col.append([i[k] for i in pages])
    for c in col:
        col_len.append(len(max(c, key=len)) + spacing)
    for l in pages:
        for k in range(0, len(pages[0])):
            while len(l[k]) < col_len[k]:
                l[k] += " "
    return render_template("index.html", pages=pages)


if __name__ == "__main__":
    # The following line enables local debugging in Flask, when the application
    # is started without a Docker container, and an uWSGI + nginx setup.
    # Furthermore it starts the local flask web server.
    app.run(host="0.0.0.0", threaded=True, debug=True, port=get_config().PORT)

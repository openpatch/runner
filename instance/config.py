import os

import docker


def get_config():
    # Set the current config:
    return BasicConfig


class BasicConfig:
    """Parent configuration class."""

    # BasicAuth protects the diagnostic's webpage.
    # There is only one user, 'admin'
    BASIC_AUTH_USERNAME = "admin"
    # The password is provided as an environment variable.
    # If the variable is not provided the authentification will not work.
    BASIC_AUTH_PASSWORD = os.getenv("OPENPATCH_BASICAUTH_SECRET")
    # A description string for the protected webpage.
    BASIC_AUTH_REALM = "commoop-runner administrative interface."

    # The Docker.py-Client which is used to run containers.
    # Make sure to run the container with '$ docker run -v /var/run/docker.sock:/var/run/docker.sock', so that the
    # client of the host is passed into the commoop-runner-container.
    DOCKER_CLIENT = docker.DockerClient(
        base_url="unix://var/run/docker.sock", version="auto"
    )

    # We wait RUNNER_START_WAIT-seconds for every started runner to respond and be ready to serve requests:
    RUNNER_STARTUP_WAIT = 5

    # This is the default timeout-value for all requests of commoop-runner to its specialized runners, if no other
    # timeout is provided.
    RUNNER_DEFAULT_TIMEOUT = 20

    # The name of the Docker network which exists on the host, to which all runners will connect to.
    # This container, i.e. COMMOOP-Runner must have access to it!
    RUNNER_NETWORK_NAME = "openpatch-runner-net"

    # Runners default memory limit as string
    RUNNER_MEM_LIMIT = os.getenv("OPENPATCH_MEM_LIMIT", "250m")

    # CPU-Limits (set to 'None' for default values):
    # (int) – The length of a CPU period in microseconds.
    RUNNER_CPU_PERIOD = os.getenv("OPENPATCH_CPU_PERIOD")
    # (int) – Microseconds of CPU time that the container can get in a CPU period.
    RUNNER_CPU_QUOTA = os.getenv("OPENPATCH_CPU_QUOTA")
    # (int) – CPU shares (relative weight).
    RUNNER_CPU_SHARES = os.getenv("OPENPATCH_CPU_SHARES")

    #
    # The following options are not meant to be changed, with the exception, that you are currently debugging.
    #
    # CSRF and CORS is needed for AJAX-requests in the diagnostic interface.
    CSRF_ENABLED = True
    CORS_HEADERS = "Access-Control-Allow-Origin"

    # The HMAC-key is set as an environment variable
    OPENPATCH_HMAC_SECRET = os.getenv("OPENPATCH_HMAC_SECRET")

    # The global LOGGER- and client-object will be stored in this config at this location during runtime. Don't change.
    LOGGER = None

    # The port, which is used for unittests and flask. This is not the port which is used in production via nginx.
    PORT = 5555
